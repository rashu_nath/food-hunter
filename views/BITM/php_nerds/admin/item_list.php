<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 01-03-17
 * Time: 23.59
 */

require_once ("../../../../vendor/autoload.php");

$objectAddItem = new \App\Admin\AddItem();

$all_items = $objectAddItem->all_item();
$trashed_items = $objectAddItem->trashed_items();

$all_items_number = count($all_items);
$all_trashed_items = count($trashed_items);


if(isset($_REQUEST['search'])){

    $all_items = $objectAddItem->search($_POST);
    $all_keywords = $objectAddItem->get_all_keywords();
    $comma_separated_keywords = '"'.implode('","',$all_keywords).'"';
}


if(isset($_REQUEST['search'])){

    $all_items = $objectAddItem->search($_POST);
}
?>
<!doctype html>
<head>
    <title>
        Active items list
    </title>

    <link rel="stylesheet" href="../../../../resources/bootstrap/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="../../../../resources/bootstrap/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="../../../../resources/bootstrap/css/bootstrap-theme.min.css">
    <!--<link rel="stylesheet" href="../../../../resources/style.css"> -->
    <script src="../../../../resources/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../../resources/bootstrap/css/jquery-ui.css">
    <script src="../../../../resources/bootstrap/js/jquery.js"></script>
    <script src="../../../../resources/bootstrap/js/jquery-ui.js"></script>

    <style>
        #items_table{
            margin: 50px;
            padding: 10px;
        }
        h2{
            text-align: center;
            font-family: monospace;
            font-weight: 600;
        }
    </style>


</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.dashboard.php">Dashboard</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="admin_profile.php">Profile</a></li>
                <li><a href="#">Help</a></li>
            </ul>
            <form class="navbar-form navbar-right" name="search_form" id="search_form">
                <input type="text" class="form-control" placeholder="Search...">
                <input hidden type="submit" name="search_submit">
            </form>
        </div>
    </div>
</nav>
<div class="table-responsive" id="items_table">
            <!--<h1 class="page-header">Dashboard</h1>

            <div class="row placeholders">
                <div class="col-xs-6 col-sm-3 placeholder">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                    <h4>Label</h4>
                    <span class="text-muted">Something else</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                    <h4>Label</h4>
                    <span class="text-muted">Something else</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                    <h4>Label</h4>
                    <span class="text-muted">Something else</span>
                </div>
                <div class="col-xs-6 col-sm-3 placeholder">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                    <h4>Label</h4>
                    <span class="text-muted">Something else</span>
                </div>
            </div>-->



            <h2 class="sub-header">Active Items List<span class="badge"> <?php echo $all_items_number;?> </span></h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Item Name</th>
                        <th>Item Ingredients</th>
                        <th>Item Picture</th>
                        <th>Item Price</th>
                        <th> Item Rating </th>
                        <th>Edit/Update</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $id="";
                    $serial = 1;
                    $background_color = "";
                    foreach($all_items as $item_field){
                        if($serial%2) $background_color = "#eeeeee";
                        else $background_color = "#ffffff";
                        echo "<tr style='background-color: $background_color'>
                                <td style='width: '>$serial</td>
                                <td style='width: '>$item_field->item_name</td>
                                <td style='width: '>$item_field->item_ingredients</td>
                                <td ><img src='uploaded_files/$item_field->item_picture' alt='$item_field->item_picture' style='width: 200px; height:100px;'> </td>
                                <td style=>$item_field->item_price</td>
                                <td> $item_field->rating </td>
                                ";
                        $serial++;
                        echo "<td style=> <a class='btn btn-default' href='item_edit.php?item_id=$item_field->item_id'>Edit</a>
                                    <a class='btn btn-default' href='item_soft_delete.php?item_id=$item_field->item_id'>Soft Delete</a>
                                     <a class='btn btn-default' href='item_email.php?item_id=$item_field->item_id'>Email this item!</a> </td></tr>";
                    }

                    ?>


</tbody>
</table>
</div>


</div>

<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
</body>