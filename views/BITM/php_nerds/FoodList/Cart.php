<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 05-03-17
 * Time: 10.40
 */

namespace App\Foodlist;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;

class Cart extends DB
{
    private $order_id;
    private $email;
    private $food_name;
    private $total_price;

    public function set_data($post_data){
        if(array_key_exists('order_id',$post_data)){
            $this->order_id = $post_data['order_id'];
        }

        if(array_key_exists('email',$post_data)){
            $this->email = $post_data['email'];
        }

        if(array_key_exists('food_name',$post_data)){
            $this->food_name = implode(',',$post_data['check_list']);
        }

        if(array_key_exists('total_price',$post_data)){
            $this->total_price = $post_data['total_price'];
        }
    }

    public function store(){
        $array_data = array($this->email, $this->food_name, $this->total_price);
        $sql = "INSERT INTO `order_list`(email, food_name, total_price) VALUES (?,?,?)";
        $sth = $this->DBH->prepare($sql);
        $status = $sth->execute($array_data);

        if($status){
            echo "data stored!";
        }
    }
}