<?php
require_once("../../../../vendor/autoload.php");
if(!isset($_SESSION)){
    session_start();
}
$objHobby = new \App\Foodlist\Foodlist();
$allData = $objHobby->index();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food List</title>

    <link rel="stylesheet" href="../../../../resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../../resources/bootstrap/css/bootstrap.min.css">
    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #111;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}

        .Form{margin:20px 150px;}
        .container-fluid {
            padding-top: 70px;
            padding-bottom: 70px;
        }


        table{
            width:600px;
        }
        td,th{width:150px;}
        .checkBox{
            margin: 0 4%;
        }
    </style>
</head>
<body><div class="container bg-1">
    <h1 style="color:#2f2f2f" class="text-center">Food List</h1>
    <div class="form-group Form text-center">
        <form action="store.php" method="post">





            <table class="table table-striped">

                <tr class="bg-3">
                    <th>Index</th>
                    <th>Food Name</th>
                    <th>Price</th>
                    <th> Quantity</th>
                    <th>Add to cart</th>
                </tr>
                <?php
                $serial=1;
                foreach ($allData as $oneData) {

                    if($serial%2){
                        $bgColor = "#eee";
                    }else{
                        $bgColor = "#ddd";
                    }
                    $name="hobby";
                    echo "
            <tr style='background-color: $bgColor' class='bg-4'>
                <td>$serial</td>
                <td>$oneData->item_name</td>
                <td>$oneData->item_price</td>
                <td> <input type='number' name='quantity'></td>
                <td><input type='checkbox' name='check_list[]' value=$oneData->item_name> </td>
            </tr>
        ";
                    $serial++;
                }

                ?>
            </table>


            <input type="submit" class="btn btn-primary" value="Add to Cart">
        </form>
    </div>
</div>

</body>
</html>