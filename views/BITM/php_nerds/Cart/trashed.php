<?php
require_once("../../../../vendor/autoload.php");
if(!isset($_SESSION)){
    session_start();
}
$objHobby = new \App\Cart\Cart();
$allData = $objHobby->index();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cart</title>

    <link rel="stylesheet" href="../../../../Resources/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../../Resources/bootstrap/css/bootstrap.min.css">
    <style>
        body {
            font: 20px Montserrat, sans-serif;
            line-height: 1.8;
            color: #111;
        }
        p {font-size: 16px;}
        .margin {margin-bottom: 45px;}

        .Form{margin:20px 150px;}
        .container-fluid {
            padding-top: 70px;
            padding-bottom: 70px;
        }


        table{
            width:600px;
        }
        td,th{width:150px;}
        .checkBox{
            margin: 0 4%;
        }
    </style>
</head>
<body><div class="container bg-1">
    <a href="../../../../src/BITM/php_nerds/FoodList/create.php " class="btn btn-primary role=" button"> View item list </a>&nbsp;&nbsp;&nbsp;

    <h1 style="color:#2f2f2f" class="text-center">Cart for ID-44</h1>
    <div class="form-group Form text-center">
        <form action="../Cart/store2.php" method="post">



            <h3>Please order Foods: </h3>



            <table class="table table-striped">

                <tr class="bg-3">
                    <th>Index</th>
                    <th>Food Name</th>
                    <th>Quantity</th>


                    <th>select</th>
                    <th>Delete</th>
                </tr>
                <?php
                $serial=1;
                foreach ($allData as $oneData) {

                    if($serial%2){
                        $bgColor = "#eee";
                    }else{
                        $bgColor = "#ddd";
                    }
                    $name="hobby";
                    echo "
            <tr style='background-color: $bgColor' class='bg-4'>
                <td>$serial</td>

                <td>$oneData->food_name </td>

                <td>$oneData->quantity</td>
                <input type='hidden' name='quantity[]' value='$oneData->quantity'>
                 <td><input type='checkbox' name='food_name[]' value='$oneData->food_name'> </td>
                <td><a href='delete.php?id=$oneData->cart_id' class='btn btn-danger'>Delete</a></td>




            </tr>
        ";
                    $serial++;
                }

                ?>
            </table>


            <a href="trashed.php" ><input type="submit" class="btn btn-primary" value="order"></a>
        </form>
    </div>
</div>

</body>
</html>