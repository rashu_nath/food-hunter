<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 3/9/2017
 * Time: 9:04 PM
 */
if(!isset($_SESSION))session_start();
require_once("../../../../vendor/autoload.php");

$obj = new \App\User\User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$pictureName = $singleUser->name.".png";

$source = $_FILES['profile_picture']['tmp_name'];
$destination = "../../../../UploadedFile/$pictureName";
move_uploaded_file($source,$destination);

$_POST['name'] = $pictureName;
$obj->setData($_POST);
$obj->profileUpdate();


