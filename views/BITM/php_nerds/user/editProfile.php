<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 3/9/2017
 * Time: 8:48 PM
 */
if(!isset($_SESSION))session_start();
require_once ('../../../../vendor/autoload.php');

$obj = new \App\User\User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$objAuth = new \App\User\Auth();
$status = $objAuth->logged_in();

if(!$status){
    header("Location:Login.php");
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Details</title>
</head>
<body>
<img src="../../../../UploadedFile/<?php echo $singleUser->profile_picture; ?>" alt="Profile Picture" width="100px" height="100px"><br/>
User Name: <?php echo $singleUser->name; ?> <a href="edit.php"> Edit</a><br/>
Email: <?php echo $singleUser->email ?><a href="edit.php"> Edit</a><br/>
Gender:<?php echo $singleUser->gender ?><a href="edit.php"> Edit</a><br/>
Mobile Number:<?php echo $singleUser->mobile_number ?><a href="edit.php"> Edit</a><br/>
Address:<?php echo $singleUser->address ?><a href="edit.php"> Edit</a><br/>
<form action="edit.php" method="post" enctype="multipart/form-data">
    Update Profile Picture:
    <input type="file" name="profile_picture">
    <input type="submit">

</form>
</body>
</html>
