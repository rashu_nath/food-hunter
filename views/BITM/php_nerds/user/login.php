<?php
if(!isset($_SESSION )){
    session_start();
}
require_once ("../../../../vendor/autoload.php");

$objAuth = new \App\User\Auth();
$status = $objAuth->logged_in();
if($status){
    header("Location:profile.php");
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body><header>Login</header><br/>
<form action="authentication.php" method="post">
    <label>Email</label><br/>
    <input type="email" name="email" required><br/>

    <label>Password</label><br/>
    <input type="password" name="password" required><br/>
    <input type="submit" value="submit">
</form>
<a href="forgotten.php">Forget Password</a>
</body>
</html>
