<?php
/**
 * Created by PhpStorm.
 * User: Rajesh Kumar Nath
 * Date: 04-03-17
 * Time: 21.09
 */
if(!isset($_SESSION )) session_start();
require_once ('../../../../vendor/autoload.php');


$objAuth = new \App\User\Auth();
$objAuth->log_out();

session_destroy();
session_start();

header("Location:login.php");