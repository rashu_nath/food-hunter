<?php
/**
 * Created by PhpStorm.
 * User: Rajesh Kumar Nath
 * Date: 03-03-17
 * Time: 20.45
 */

namespace App\User;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

use App\Model\Database as DB;

class User extends DB
{
    private $id;
    private $name;
    private $email;
    private $password;
    private $gender;
    private $mobile_number;
    private $address;
    private $email_token;


    public function setData($postData){
        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('email',$postData)){
            $this->email = $postData['email'];
        }

        if(array_key_exists('password',$postData)){
            $this->password = $postData['password'];
        }

        if(array_key_exists('gender',$postData)){
            $this->gender = $postData['gender'];
        }

        if(array_key_exists('mobile_number',$postData)){
            $this->mobile_number = $postData['mobile_number'];
        }

        if(array_key_exists('address',$postData)){
            $this->address = $postData['address'];
        }

        if(array_key_exists('email_token',$postData)){
            $this->email_token = $postData['email_token'];
        }
    }

    public function email_exist(){
        $query = "SELECT * FROM `user` WHERE `email`='$this->email'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function store(){
        $dataArray = array($this->name,$this->email,$this->password,$this->gender,$this->mobile_number,$this->address,$this->email_token);
        $sql = "INSERT INTO user(name,email,password,gender,mobile_number,address,email_verified) VALUES(?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Please, login here!");
            Utility::redirect($_SERVER['HTTP_REFERER']);
        }
        else{
            Message::message("Your email already been taken! Please, try another :) ");
            Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function view(){
        $query = "SELECT * FROM `user` WHERE `email` = '$this->email'";
        $STH = $this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function validTokenUpdate(){
        $query = "UPDATE `user` SET `email_verified` = '".'Yes'."' WHERE `email` = '$this->email'";
        $result = $this->DBH->prepare($query);
        $result->execute();

        if(!$result){
            echo "Error";
        }
        else{
            header("Location:../../../../views/BITM/php_nerds/user/login.php");
        }
    }

    public function profileUpdate(){
        $query = "UPDATE `user` SET `profile_picture` = '".$this->name."' WHERE `email` = '$this->email'";
        $result = $this->DBH->prepare($query);
        $result->execute();

        if(!$result){
            echo "Error";
        }
        else{
            $http_refferer = $_SERVER['HTTP_REFERER'];
            header("Location:$http_refferer");

        }
    }


}