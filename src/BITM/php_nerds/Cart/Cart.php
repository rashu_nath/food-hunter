<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/30/2017
 * Time: 2:09 PM
 */

namespace App\Cart;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Cart extends DB
{
    private $id;
    private $user_id;
    private $cart_id;
    private $food_name;
    private $quantity;

    public function setData($allPostData=null){

        if(array_key_exists("id", $allPostData)){
            $this->id= $allPostData['id'];

        }
        if(array_key_exists("user_id", $allPostData)){
            $this->user_id= $allPostData['user_id'];

        }
        if(array_key_exists("cart_id", $allPostData)){
            $this->cart_id= $allPostData['cart_id'];

        }
        if(array_key_exists('quantity',$allPostData)){
            $this->quantity = implode(',',$allPostData['quantity']);
        }

        if(array_key_exists('food_name',$allPostData)){
            $this->food_name = implode(',',$allPostData['food_name']);
        }
        // $this->food_name= $allPostData['hobby1'].",".$allPostData['hobby2'].",".$allPostData['hobby3'].",".$allPostData['hobby4'].",".$allPostData['hobby5'].",".$allPostData['hobby6'];

    }


    public function store2(){
        $this->user_id=44;
        $arrayData = array($this->user_id,$this->food_name,$this->quantity);

        $query = 'INSERT INTO order_list (user_id,food_name,quantity) VALUE (?,?,?)';


        $STH =$this->DBH->prepare($query);


        $result = $STH->execute($arrayData);


        //  if($result){
        //  Message::setMessage("Success! Data has been inserted successfully");
        //}else{
        // Message::setMessage("Failed! Data has not been inserted");
        //  }

        Utility::redirect('trashed.php');
    }

    public function index(){
        $sql = "Select * from cart where user_id='44'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){
        $sql = "Select * from Cart ";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function delete(){
        $sql = "Delete from cart WHERE cart_id=".$this->id;
        $result =$this->DBH->exec($sql);

        if($result){
            Message::setMessage(" Data has been deleted successfully");
        }else{
            Message::setMessage(" Data has not been deleted");
        }
        Utility::redirect('trashed.php');

    }


    public function view(){
        $sql = "Select * from cart where cart_id='".$this->id."'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


        Utility::redirect('trashed.php');
    }
}