<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/30/2017
 * Time: 2:09 PM
 */

namespace App\Foodlist;

if(!isset($_SESSION)) session_start();

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Foodlist extends DB
{
    private $email;
    private $food_name;
    private $quantity;  //edited by rashu

    public function setData($allPostData=null){

        if(array_key_exists("id", $allPostData)){
            $this->id= $allPostData['id'];

        }
        if(array_key_exists("email", $allPostData)){
            $this->email= $allPostData['email'];

        }

        if(array_key_exists("quantity", $allPostData)){
            $this->quantity = $allPostData['quantity']; //edited by rashu
        }

        if(array_key_exists('check_list',$allPostData)){
            $this->food_name = implode(',',$allPostData['check_list']);
        }
           // $this->food_name= $allPostData['hobby1'].",".$allPostData['hobby2'].",".$allPostData['hobby3'].",".$allPostData['hobby4'].",".$allPostData['hobby5'].",".$allPostData['hobby6'];

    }


    public function store(){
        $arrayData = array($this->email, $this->food_name, $this->quantity);
        $query = 'INSERT INTO cart (user_email, food_name, quantity) VALUES (?,?,?)';


        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);


        if($result){
          //  Message::setMessage("Success! Data has been inserted successfully");
        }else{
           // Message::setMessage("Failed! Data has not been inserted");
        }

        Utility::redirect('make_order.php');
    }

    public function view_cart($post_data){
        $sql = "SELECT * FROM `cart` WHERE `user_email` LIKE '".$post_data['email']."'";
        $sth = $this->DBH->query($sql);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }



    public function index(){
        $sql = "Select * from items";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function trashed(){
        $sql = "Select * from hobby_info where soft_delete='yes'";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view(){
        $sql = "Select * from hobby_info where id=".$this->id;
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


        Utility::redirect('index.php');
    }
}